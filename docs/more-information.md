Amtk - more information
=======================

About versions
--------------

Amtk follows the even/odd minor version scheme.

For example the `5.1.x` versions are unstable (development versions), and the
`5.2.x` versions are stable.

Dependencies
------------

- GLib
- GTK 3

Documentation
-------------

See the `gtk_doc` Meson option. A convenient way to read the API documentation
is with the [Devhelp](https://wiki.gnome.org/Apps/Devhelp) application.

See also other files in this directory for additional notes.

Development resources
---------------------

- [Tarballs](https://download.gnome.org/sources/amtk/)

Development and maintenance
---------------------------

If you want to see Amtk improved, you are encouraged to contribute. The
maintainer will do its best at reviewing merge requests. However, contributions
need to follow the roadmap of Amtk or the general philosophy of the project.
